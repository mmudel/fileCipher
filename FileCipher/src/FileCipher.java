import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

public class FileCipher{

	public static void main(String[] args){
		new MainWindow();
	}
}

class MainWindow{
	private JFrame okno;
	private JTextField fileField;
	private JTextField keyField;
	private JButton encryptButton;
	private JButton decryptButton;
	private JButton chooseFileButton;
	private JLabel fileLabel;
	private JLabel keyLabel;
	private JPanel informationPanel;
	private JPanel actionPanel;
	private JPanel chooseFilePanel;
	private boolean failFlag = false;
	private List<String> errors = new ArrayList<String>();
	
	public MainWindow(){
		init();
		okno.setVisible(true);
	}
	
	private void init(){
		okno = new JFrame("File Encrypter");
		fileField = new JTextField(35);
		keyField = new JTextField(35);
		encryptButton = new JButton("Encrypt");
		decryptButton = new JButton("Decrypt");
		chooseFileButton = new JButton("Choose file...");
		fileLabel = new JLabel("Choose file");
		keyLabel = new JLabel("Enter key");
		informationPanel = new JPanel();
		actionPanel = new JPanel();
		chooseFilePanel = new JPanel();
		
		okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		okno.setSize(new Dimension(500, 250));
		okno.setLocationRelativeTo(null);
		okno.setResizable(false);
		
		encryptButton.addActionListener(encryptListener);
		
		decryptButton.addActionListener(decryptListener);
		
		chooseFileButton.addActionListener(chooseFileListener);
		chooseFileButton.setPreferredSize(new Dimension(35, 20));
		
		chooseFilePanel.setLayout(new FlowLayout(FlowLayout.LEADING));
		chooseFilePanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		chooseFilePanel.add(fileField);
		chooseFilePanel.add(chooseFileButton);
		
		informationPanel.setLayout(new GridLayout(4, 1));
		informationPanel.setBorder(BorderFactory.createEmptyBorder(10, 30, 20, 30));
		informationPanel.add(fileLabel);
		informationPanel.add(chooseFilePanel);
		informationPanel.add(keyLabel);
		informationPanel.add(keyField);
		
		actionPanel.setLayout(new GridLayout(1, 2));
		actionPanel.setBorder(BorderFactory.createEmptyBorder(10, 30, 20, 30));
		actionPanel.add(encryptButton);
		actionPanel.add(decryptButton);
		
		okno.getContentPane().add(informationPanel, BorderLayout.NORTH);
		okno.getContentPane().add(actionPanel, BorderLayout.SOUTH);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Throwable e){
			System.out.println(e);
		}
	}
	
	private void encrypt(byte[] keyBytes, Path file){
		byte[] fileData;
		byte[] encryptedFileData;
		byte newByte[];
		if(file.toFile().isDirectory()){
			for(File nextFile : file.toFile().listFiles()){
				encrypt(keyBytes, nextFile.toPath());
			}
		}
		else{
			try{
				fileData = Files.readAllBytes(file);
				encryptedFileData = new byte[fileData.length];
				for(int i=0; i < fileData.length; i++){
					newByte = ByteBuffer.allocate(4).putInt(fileData[i] ^ keyBytes[i%keyBytes.length]).array();
					encryptedFileData[i] = newByte[3];
				}
			}
			catch(IOException e){
				failFlag = true;
				errors.add("Problems while reading " + file);
				return;
			}
			Path newFile = Paths.get(file + ".enc");
			try{
				Files.write(newFile, encryptedFileData);
			}
			catch(IOException e){
				failFlag = true;
				errors.add("Problems while creating encrypted file " + newFile);
				return;
			}
			try {
				Files.delete(file);
			} 
			catch (IOException e) {
				failFlag = true;
				errors.add("Problems while deleting old file " + newFile);
				return;
			}
		}
	}
	
	private void decrypt(byte[] keyBytes, Path file){
		byte[] fileData;
		byte[] decryptedFileData;
		byte[] newByte;
		if(file.toFile().isDirectory()){
			for(File nextFile : file.toFile().listFiles()){
				decrypt(keyBytes, nextFile.toPath());
			}
		}
		else{
			if(!file.toString().substring(file.toString().length()-4).equals(".enc")){
				failFlag = true;
				errors.add("File " + file + " is not encrypted file!");
				return;
			}
			try{
				fileData = Files.readAllBytes(file);
				decryptedFileData = new byte[fileData.length];
				for(int i=0; i < fileData.length; i++){
					newByte = ByteBuffer.allocate(4).putInt(fileData[i] ^ keyBytes[i%keyBytes.length]).array();
					decryptedFileData[i] = newByte[3];
				}
			}
			catch(IOException e){
				failFlag = true;
				errors.add("Problems while reading " + file);
				return;
			}
			Path newFile = Paths.get(file.toString().substring(0, file.toString().length()-4));
			try{
				Files.write(newFile, decryptedFileData);
			}
			catch(IOException e){
				failFlag = true;
				errors.add("Problems while creating decrypted file " + newFile);
				return;
			}
			try {
				Files.delete(file);
			} 
			catch (IOException e) {
				failFlag = true;
				errors.add("Problems while deleting old file " + newFile);
				return;
			}
		}
	}
	
	private ActionListener encryptListener = new ActionListener(){
		public void actionPerformed(ActionEvent evt){
			String key;
			byte[] keyBytes;
			key = keyField.getText();
			if(key.length() == 0){
				JOptionPane.showMessageDialog(okno, "Key can't be empty!");
				return;
			}
			keyBytes = key.getBytes();
			if(fileField.getText().length() == 0){
				JOptionPane.showMessageDialog(okno, "No file chosen!");
				return;
			}
			Path file = Paths.get(fileField.getText());
			encrypt(keyBytes, file);
			if(failFlag == false)
				JOptionPane.showMessageDialog(okno, "File(s) succesfully encrypted!");
			else{
				Path logFile = Paths.get("errorlog.txt");
				try{
					Files.write(logFile, errors);
				}
				catch(IOException e){
					JOptionPane.showMessageDialog(okno, "Can't create log file! Program will exit");
					System.exit(1);
				}
				JOptionPane.showMessageDialog(okno, "File(s) encrypted with errors. Errors are saved in errorlog.txt");
				failFlag = false;
				errors.clear();
			}
		}
	};
	private ActionListener decryptListener = new ActionListener(){
		public void actionPerformed(ActionEvent evt){
			String key;
			byte[] keyBytes;
			key = keyField.getText();
			if(key.length() == 0){
				JOptionPane.showMessageDialog(okno, "Key can't be empty!");
				return;
			}
			keyBytes = key.getBytes();
			if(fileField.getText().length() < 5){
				JOptionPane.showMessageDialog(okno, "No file chosen! Remember that file must have .enc extension.");
				return;
			}
			Path file = Paths.get(fileField.getText());
			decrypt(keyBytes, file);
			if(failFlag == false)
				JOptionPane.showMessageDialog(okno, "File(s) succesfully decrypted!");
			else{
				Path logFile = Paths.get("errorlog.txt");
				try{
					Files.write(logFile, errors);
				}
				catch(IOException e){
					JOptionPane.showMessageDialog(okno, "Can't create log file! Program will exit");
					System.exit(1);
				}
				JOptionPane.showMessageDialog(okno, "File(s) decrypted with errors. Errors are saved in errorlog.txt");
				failFlag = false;
				errors.clear();
			}
		}
	};
	private ActionListener chooseFileListener = new ActionListener(){
		public void actionPerformed(ActionEvent evt){
			final JFileChooser fc = new JFileChooser();
			fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
			int returnVal = fc.showOpenDialog(okno);
			if(returnVal == JFileChooser.APPROVE_OPTION){
				fileField.setText(fc.getSelectedFile().getAbsolutePath());
			}
		}
	};
}
